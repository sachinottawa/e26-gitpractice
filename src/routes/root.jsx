import React from 'react';
import { Outlet } from 'react-router-dom';

function Root(props) {
    return (
        <>
            <header></header>
            <Outlet />
            <footer></footer>
        </>
    );
}

export default Root;